from st3m.application import Application, ApplicationContext
from st3m import logging
from st3m.ui.colours import PUSH_RED, GO_GREEN, BLACK
from st3m.input import InputState
from sys_buttons import get_left, PRESSED_DOWN
from ctx import Context
from json import loads, dumps
from math import sin, pi

log = logging.Log(__name__, level=logging.INFO)

class LissajouMaschine:
	def __init__(self):
		self.reset()

	def reset(self):
		self.phi = 1.0
		self.dphi = self.phi / 2
		self.rho = pi / 2
		self.drho = self.rho / 2
		self.alpha = 0
		self.dalpha = 0.04
		self.ddalpha = self.dalpha / 2

	def ops(self, no, mode):
		if no == 0:
			if mode:
				self.phi += self.dphi
				log.info("phi=%s"% self.phi)
			else:
				self.phi -= self.dphi
				log.info("phi=%s"% self.phi)
		elif no == 1:
			if mode:
				self.dphi *= 2
				log.info("dphi=%s"% self.dphi)
			else:
				self.dphi /= 2
				log.info("dphi=%s"% self.dphi)
		elif no == 2:
			if mode:
				self.rho += self.drho
				log.info("rho=%s"% self.rho)
			else:
				self.rho -= self.drho
				log.info("rho=%s"% self.rho)
		elif no == 3:
			if mode:
				self.drho *= 2
				log.info("drho=%s"% self.drho)
			else:
				self.drho /= 2
				log.info("drho=%s"% self.drho)
		elif no == 4:
			if mode:
				self.dalpha += self.ddalpha
				log.info("dalpha=%s"% self.dalpha)
			else:
				self.dalpha -= self.ddalpha
				log.info("dalpha=%s"% self.dalpha)
		elif no == 5:
			if mode:
				self.ddalpha *= 10
				log.info("ddalpha=%s"% self.ddalpha)
			else:
				self.ddalpha /= 10
				log.info("ddalpha=%s"% self.ddalpha)

	def iter(self):
		while True:
			yield (
					int(sin(self.alpha*self.phi) * 90),
					int(sin(self.alpha+self.rho) * 90),
					)
			self.alpha += self.dalpha

class Configuration:
	def __init__(self):
		self.name = "Lissaj"

	@classmethod
	def load(cls, path):
		res = cls()
		try:
			with open(path) as f:
				data = loads(f.read())
			log.info("config loaded")
		except Exception:
			data = {}
		return res

	def save(self, path):
		with open(path, "w") as f:
			f.write(dumps(dict()))
			f.close()


class LissajouApp(Application):
	def __init__(self, app_ctx):
		super().__init__(app_ctx)
		self._filename = "/flash/lissaj.json"
		self._config = Configuration.load(self._filename)
		self.machine = LissajouMaschine()
		self.generator = self.machine.iter()
		self.dots = [(0, 0, ), ] * 100
		self.cursor = 0
		self.countdown = 300 # dont read the key that started the app

	def draw(self, ctx):
		ctx.rgb(0, .3, 0).rectangle(-120, -120, 240, 240).fill()
		for dot in self.dots:
			x, y = dot
			ctx.rgb(.9, .6, 0).rectangle(x-2, y-2, 4, 4).fill()

	def on_exit(self):
		self._config.save(self._filename)

	def think(self, ins, delta_ms):
		super().think(ins, delta_ms)
		if self.countdown <= 0:
			for i in range(10):
				petal = ins.captouch.petals[i]
				if petal.pressed:
					(rad, phi) = petal.position
					log.info("%s %s %s" % (i, rad, phi, ))
					self.machine.ops(i // 2, i % 2)
					self.countdown = 300
			if get_left() == PRESSED_DOWN:
				self.machine.reset()
				self.countdown = 300
		else:
			log.info("countdown=%d delta_ms=%d" % (self.countdown, delta_ms, ))
			self.countdown -= delta_ms
		dot = next(self.generator)
		self.dots[self.cursor] = dot
		self.cursor += 1
		self.cursor %= len(self.dots)

if __name__ == "__main__":
	import st3m.run
	st3m.run.run_view(LissajouApp(ApplicationContext()))
